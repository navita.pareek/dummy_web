const UserReducer = (state, action) => {
    switch(action.type) {
        case 'SAVE_PLAYER_NAME_SUCCESS': 
        return {
            ...state,
            playerName: action.payload.playerName        
        };
        default: return state;
    };
};

export default UserReducer;