import React, { createContext, useReducer } from 'react';
import GameReducer from './game-reducer';
import {initialState} from './initialState';

const GameContextProvider = ({children}) => {
    const [state, dispatch] = useReducer(GameReducer, initialState);
    return (
        <GameContext.Provider value={[state, dispatch]}>
            {children}
        </GameContext.Provider>
    );
};

export const GameContext = createContext(initialState);

export default GameContextProvider