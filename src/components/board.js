import React, { useContext } from 'react';
import { Box, Button } from 'grommet';
import {GameContext} from '../context/game-context/game-context-provider';
import Square from './square';
  
export default function Board() {
    const [state, dispatch] = useContext(GameContext);

    const renderSquare = (i) => <Square 
    value={state.squares[i]}
    onClick={() => handleClick(i)}/>;

    const handleClick = (i) => {
        const newSquares = state.squares.slice();
        if (calculateWinner(newSquares) || state.squares[i]) {
            return;
        }
        newSquares[i] = state.isXNext ? 'X' : 'O';
        dispatch({type: 'UPDATE_SQUARES', payload: {squares: newSquares}});
        dispatch({type: 'SWITCH_PLAYER'});
    }

    const winner = calculateWinner(state.squares);
        let status;
        if(winner) {
            status = 'Winner: ' + winner;
        } else {
            status = 'Next player:' + (state.isXNext ? 'X' : 'O');
        }

        return (
        <Box>
            <Box pad="medium">{status}</Box>
            <Box pad={{ bottom: 'medium' }}>
                <Button secondary label='Reset Board' onClick={() => dispatch({type: 'RESET_BOARD'})}></Button>
            </Box>
            <Box direction="row">
            {renderSquare(0)}
            {renderSquare(1)}
            {renderSquare(2)}
            </Box>
            <Box direction="row">
            {renderSquare(3)}
            {renderSquare(4)}
            {renderSquare(5)}
            </Box>
            <Box direction="row">
            {renderSquare(6)}
            {renderSquare(7)}
            {renderSquare(8)}
            </Box>
        </Box>
        );
}


function calculateWinner(squares) {
    const lines = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6],
    ];

    for (let i = 0; i < lines.length; i++) {
      const [a, b, c] = lines[i];
      if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
        return squares[a];
      }
    }
    return null;
}