import React, {useContext} from 'react';
import { Box, Form, FormField, Button } from 'grommet';
import {UserContext} from '../context/user-context/user-context-provider';

export default function Login() {
    const [state, dispatch] = useContext(UserContext);
    
    const login = (loginInfo) => {
        dispatch({type: 'SAVE_PLAYER_NAME', payload: {playerName: loginInfo.playerName}});
    }

    return (
        <Form onSubmit={({ value, touched }) => {
            login(value);
        }}>
            <FormField name="playerName" label="Player Name (for future use)" />
            <Box>
                <Box width="small" pad={{ top: 'medium', bottom: 'medium' }} alignSelf="end">
                    <Button type="submit" primary label="Submit" />
                </Box>
            </Box>
        </Form>
        );
}