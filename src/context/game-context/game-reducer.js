import { initialState } from './initialState';

const GameReducer = (state, action) => {
    switch(action.type) {
        case 'SWITCH_PLAYER': 
            return {
                ...state,
                isXNext: !state.isXNext
            };
        case 'UPDATE_SQUARES': 
            return {
                ...state,
                squares: action.payload.squares
            };  
        case 'RESET_BOARD': 
            return initialState;     
        default: return state;
    };
};

export default GameReducer;