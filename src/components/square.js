import React from 'react';
import { Button, Box } from 'grommet';

export default function Square(props) {
    return (
        <Box
          justify="center"
          align="center"
          height="80px"
          width="80px"
        >
        <Button color='neutral-3' size="medium" fill secondary
            onClick={props.onClick} label={props.value}/>
        </Box>
        );
}