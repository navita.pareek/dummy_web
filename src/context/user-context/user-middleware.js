import service from './user-service.js';

export const userMiddleware = (dispatch) =>  (action) => 
{   
    switch (action.type) {
        case 'SAVE_PLAYER_NAME': 
            // Test of Lambda
            (async () => {
                const response = await fetch('https://axvk10tka4.execute-api.us-east-2.amazonaws.com/test');
                const rj = await response.json();
                console.log(rj);
                if (response) {
                    dispatch({type: 'SAVE_PLAYER_NAME_SUCCESS', payload: action.payload });
                }
            })();
            break;
        default: {
            dispatch(action);
            break;
        }
    };
};