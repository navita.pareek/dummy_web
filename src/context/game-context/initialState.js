export const initialState = {
    isXNext: true,
    squares: Array(9).fill(null),
    isLoggedIn: false
};