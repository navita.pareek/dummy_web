import React from 'react';
import ReactDOM from 'react-dom';
import Game from './containers/game';
import UserContextProvider from './context/user-context/user-context-provider';
  
ReactDOM.render(
    <UserContextProvider>
        <Game />
    </UserContextProvider>,
    document.getElementById('root')
);