import React, { createContext, useReducer } from 'react';
import UserReducer from './user-reducer';
import {userMiddleware} from './user-middleware';

const initialState = {
    playerName: ""
};

const UserContextProvider = ({children}) => {
    const [state, dispatch] = useReducer(UserReducer, initialState);
    const middlerware = userMiddleware(dispatch);

    return (
        <UserContext.Provider value={[state, middlerware, dispatch ]}>
            {children}
        </UserContext.Provider>
    );
};

export const UserContext = createContext(initialState);

export default UserContextProvider