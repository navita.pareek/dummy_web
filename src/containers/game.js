import React, {useContext} from 'react';
import { Box, Grommet } from 'grommet';
import Board from '../components/board';
import GameContextProvider from '../context/game-context/game-context-provider';
import {Theme} from '../theme';
import Login from '../components/login';
import {UserContext} from '../context/user-context/user-context-provider';
import ReactPlayer from 'react-player'

export default function Game() {
    const [state] = useContext(UserContext);

    return (
        <Grommet theme={Theme}>
            <Box
                direction="row"
                pad="medium"
                background="neutral-3"
                align="center"
                justify="center"
                height="large"
                >
                    <Box pad="medium" background="light-1">
                        <ReactPlayer controls url='https://www.youtube.com/watch?v=G0jHXuIywdM' />
                    </Box>
                    
                    <Box pad="small" background="dark-1">
                        {state.playerName ? 
                        <GameContextProvider>
                            <Board />
                        </GameContextProvider> :
                        <Login/>}
                    </Box>
                </Box> 
            
        </Grommet>
    );
}